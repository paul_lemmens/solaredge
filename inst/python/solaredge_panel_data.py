#!/usr/bin/env python3

"""Python tooling for undocumented SolarEdge API endpoints.

This module is a refinement of the code found here: From https://gist.github.com/cooldil/0b2c5ee22befbbfcdefd06c9cf2b7a98
to facilitate using it in the R package solaredge in which this module is
distributed.

Functions:
    check_solaredge_cookie() to check for or create the required cookie to
        connect with the undocumented API endpoint.
    download_panel_data() to download the panels' production data over time.
    download_panel_layout() to download the panels' layout to support naming
        panels in other R package functions.
    _get_time_zone() to extract time zone information from a specific site.
    _load_api_keys() for loading the required authentication material.
"""

import argparse, requests, pickle, json, pytz, os
import pandas as pd
from pathlib import Path
from datetime import datetime
from dotenv import load_dotenv, dotenv_values

def _load_api_keys():
    """Use dotenv to load the api_key and other credentials."""

    _env_file = os.path.expanduser('~/.env')
    api_values = dotenv_values(_env_file)

    user = api_values['SOLAREDGE_USER'] # web username
    pwd = api_values['SOLAREDGE_PWD'] # web password
    site_id = api_values['SOLAREDGE_ID'] # site id

    return user, pwd, site_id


# Set up argument parser in case the module is used from the commandline.
_parser = argparse.ArgumentParser(
    prog='SolarEdge Panel Data',
    description="Supports downloading individual panels' power production data.")

_parser.add_argument('dest_path',
                     help='Destination path for downloaded files (default: current working directory).',
                     default=os.getcwd(), nargs='?'   # Makes the argument optional.
                     )


def _get_time_zone():
    """Get the time zone for a specific site from the API."""
    user, pwd, site_id = _load_api_keys()
    url = f"https://monitoringapi.solaredge.com/site/{site_id}/details"

    # Abuse the .Renviron file as input for dotenv, but now make sure to not
    # create environment variables.
    _Renv_file = os.path.expanduser('~/.Renviron')
    env_values = dotenv_values(_Renv_file)
    payload = {'api_key': env_values['SOLAREDGE_KEY']}

    req = requests.get(url, params=payload)
    # Adhoc fix to get code running.
    if req.status_code != 400:
        res = 'Europe/Amsterdam'
    else:
        res = req.json()
        res = res['details']['location']['timeZone']

    return res


def check_solaredge_cookie(dest_path=os.getcwd()):
    """Check if a cookie to the SolarEdge monitoring website exists and creates it if not.

    Parameters:
        dest_path Directory where to store the cookie representation on disk.
            Defaults to current working directory
    Returns:
        A requests.session object.
    """
    _SOLAREDGE_USER, _SOLAREDGE_PASS, _SOLAREDGE_SITE_ID = _load_api_keys()
    _login_url = "https://monitoring.solaredge.com/solaredge-apigw/api/login"
    session = requests.session()

    p = Path(dest_path)
    cookie_file = p/Path('solaredge_panel_data.cookies')
    try:  # Make sure the cookie file exists
        with open(cookie_file, 'rb') as f:
            f.close()
    except IOError:  # Create the cookie file
        session.post(_login_url, headers = {"Content-Type": "application/x-www-form-urlencoded"}, data={"j_username": _SOLAREDGE_USER, "j_password": _SOLAREDGE_PASS})
        with open(cookie_file, 'wb') as f:
            pickle.dump(session.cookies, f)
            f.close()

    return session, cookie_file



def download_panel_data(sess, cookie, dest_path=os.getcwd(), tz=_get_time_zone()):
    """Dowload individual panel's power production data from the web API.

    Accesses a hidden/unexposed endpoint of the SolarEdge API that enables
    downloading the panels' individual production data over time. This
    undocumented endpoint takes a parameter that specifies the time range for
    which to download the data. This is either daily or weekly.

    Historic data cannot be downloaded, so to gather full data over a longer
    period of time, this function has to be called every week to download the
    last weekly data, for instance, using a cron job.

    Parameters:
        sess: A requests.session object created by the function checking the
            required cookie
        cookie: A directory and file for the cookie to be used for connecting to
            the API.
        dest_path Directory where to store the panel data on disk. Defaults
            to current working directory
        tz Time zone in which the data is recorded.

    Returns:
        A data frame with the downloaded data. As side effect these data are
            also written on disk.
    """
    _SOLAREDGE_USER, _SOLAREDGE_PASS, _SOLAREDGE_SITE_ID = _load_api_keys()
    _panels_url = "https://monitoring.solaredge.com/solaredge-web/p/playbackData"
    _DAILY_DATA = "4"
    _WEEKLY_DATA = "5"
    _TIME_UNIT = _WEEKLY_DATA

    with open(cookie, 'rb') as f:
        sess.cookies.update(pickle.load(f))
        panels = sess.post(_panels_url, headers = {"Content-Type": "application/x-www-form-urlencoded", "X-CSRF-TOKEN": sess.cookies["CSRF-TOKEN"]}, data={"fieldId": _SOLAREDGE_SITE_ID, "timeUnit": _TIME_UNIT})
        if panels.status_code != 200:
            sess.post(_login_url, headers = {"Content-Type": "application/x-www-form-urlencoded"}, data={"j_username": _SOLAREDGE_USER, "j_password": _SOLAREDGE_PASS})
            panels = sess.post(_panels_url, headers = {"Content-Type": "application/x-www-form-urlencoded", "X-CSRF-TOKEN": sess.cookies["CSRF-TOKEN"]}, data={"fieldId": _SOLAREDGE_SITE_ID, "timeUnit": _TIME_UNIT})
            if panels.status_code != 200:
                exit()
            with open(cookie, 'wb') as f:
                pickle.dump(s.cookies, f)
        response = panels.content.decode("utf-8").replace('\'', '"').replace('Array', '').replace('key', '"key"').replace('value', '"value"')
        response = response.replace('timeUnit', '"timeUnit"').replace('fieldData', '"fieldData"').replace('reportersData', '"reportersData"')
        response = json.loads(response)

        data = {}
        for date_str in response["reportersData"].keys():
            date = datetime.strptime(date_str, '%a %b %d %H:%M:%S GMT %Y')
            date = pytz.timezone(tz).localize(date).astimezone(pytz.utc)
            for sid in response["reportersData"][date_str].keys():
                for entry in response["reportersData"][date_str][sid]:
                    if entry["key"] not in data.keys():
                        data[entry["key"]] = {}
                    data[entry["key"]][date] = float(entry["value"].replace(",", ""))

        df = pd.DataFrame(data)

        fn = datetime.now(pytz.utc).strftime('%Y%m%d-%H%M%S')
        fn = 'solaredge_panel_data_' + fn + '.csv'
        fn = Path(dest_path)/Path(fn)
        df.to_csv(fn)

        return df



def download_panel_layout(dest_path=os.getcwd()):
    """Download the panel's layout.

    Uses another undocumented API endpoint that is used (only) in the official
    SolarEdge Monitoring site. This endpoint produces not only the current
    production numbers per panel but also their layout, including data like
    serial numbers. These serial numbers can later be used for naming the
    panels because the download_panel_data() reports its data per panel serial
    number.

    Compared to its counterpart download_panel_data(), this function is designed
    more to be called interactively because it implements the full chain of steps
    of checking the cookie and then downloading the data in one go.

    Also note that this function primarily exists for being called (via reticulate)
    in R to be able to set better PV panel names. Therefore it directly returns the
    complicated json output that can be easily processed in R.

    Parameters:
        dest_path Directory where to store the panel data on disk. Defaults
            to current working directory

    Returns:
        A json string representing the output of the endpoint (utf8 decoded)
        and with corrections applied to make it valid json. Also writes this
        data to disk.
    """
    _SOLAREDGE_USER, _SOLAREDGE_PASS, _SOLAREDGE_SITE_ID = _load_api_keys()
    _layout_url = f"https://monitoring.solaredge.com/solaredge-apigw/api/sites/{_SOLAREDGE_SITE_ID}/layout/logical"

    # Use simple authentication, via
    # https://github.com/RDols/DomoticzDzVentsScripts/blob/master/dzRequestSolarInfo/dzRequestSolarInfo.lua
    panels = requests.get(_layout_url, auth = (_SOLAREDGE_USER, _SOLAREDGE_PASS))
    response = panels.content.decode("utf-8").replace('"\'', '"').replace('\'"', '"')

    fn = datetime.now(pytz.utc).strftime('%Y%m%d-%H%M%S')
    fn = Path('solaredge_panel_layout_' + fn + '.json')
    fn = Path(dest_path)/fn
    with open(fn, 'w') as outfile:
        outfile.write(response)

    return response



if __name__ == '__main__':
    args = _parser.parse_args()

    session, cf = check_solaredge_cookie(dest_path=args.dest_path)
    download_panel_data(sess = session, cookie=cf, dest_path=args.dest_path)
