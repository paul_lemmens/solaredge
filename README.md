# solaredge

`solaredge` is an R package that wraps the API that the SolarEdge company provides to its users of its
photovoltaic (PV; solar) panels. Additionally, it includes an R Shiny dashboard for visualization and analysis of the
data provided by the API.

# Support

Use the issue tracker for support of and questions about the package.

# Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

