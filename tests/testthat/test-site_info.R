test_that('Site list is downloaded with correct names', {

  res_names <- c(
    "id",                "name",              "account_id",
    "status",            "peak_power",        "last_update_time",
    "currency",          "installation_date", "pto_date",
    "notes",             "type",              "country",
    "city",              "address",           "address_2",
    "zip",               "time_zone",         "country_code",
    "manufacturer_name", "model_name",        "maximum_power",
    "temperature_coef",  "site_image",        "data_period",
    "installer_image",   "details",           "overview",
    "is_public"
  )

  expect_equal(names(solaredge_site_list()), res_names)
})


test_that('Site details are downloaded with correct names', {

  res_names <- c(
    "id",                "name",              "account_id",
    "status",            "peak_power",        "last_update_time",
    "currency",          "installation_date", "pto_date",
    "notes",             "type",              "location_country",
    "location_city",     "location_address",  "location_address_2",
    "location_zip",      "location_time_zone","location_country_code",
    "primary_module_manufacturer_name",       "primary_module_model_name",
    "primary_module_maximum_power",           "primary_module_temperature_coef",
    "uris_site_image",   "uris_data_period",  "uris_installer_image",
    "uris_details",      "uris_overview",
    "public_settings_is_public"
  )

  expect_equal(names(solaredge_site_details()), res_names)
})


test_that('Site overview is correctly retrieved', {

  res_names <- c('last_update_time', 'lifetime_data', 'lifetime_revenue',
                 'last_year_data', 'last_month_data', 'last_day_data',
                 'current_power', 'measured_by')

  expect_equal(names(solaredge_site_overview()), res_names)
})


test_that('Environmental benefits are correctly downloaded', {

  res_names <- c('trees_planted', 'light_bulbs', 'gas', 'emission', 'units')
  expect_equal(names(solaredge_site_envbenefits()), res_names)

  res <- solaredge_site_envbenefits()
  expect_equal(res$units[1], 'kg')
  res <- solaredge_site_envbenefits(unit = 'Imperial')
  expect_equal(res$units[1], 'lb')
})

test_that('Site energy checks API usage limitations', {

  expect_error(solaredge_site_energy('2021-01-01', '2023-01-01', time_unit = '15m'))
  expect_error(solaredge_site_energy('2021-01-01', '2023-01-01', time_unit = '1d'))
  expect_error(solaredge_site_energy('2023-01-01', '2023-03-01', time_unit = '15m'))

  expect_error(solaredge_site_energy('2021-01-01', '2023-01-01', time_unit = '1w'))
  expect_error(solaredge_site_energy('2021-01-01', '2023-01-01', time_unit = '1m'))
  expect_error(solaredge_site_energy('2021-01-01', '2023-01-01', time_unit = '1y'))

  expect_no_error(solaredge_site_energy('2023-01-01', '2023-01-28', time_unit = '15m'))
  expect_no_error(solaredge_site_energy('2023-01-01', '2023-01-28', time_unit = '1h'))
  expect_no_error(solaredge_site_energy('2023-01-01', '2023-01-28', time_unit = '1d'))
})

test_that('Site power checks API usage limitations', {

  expect_error(solaredge_site_power('2023-01-01 14:14:14', '2023-03-01 14:14:14'))
  expect_no_error(solaredge_site_power('2023-01-01 14:14:14', '2023-01-28 14:14:14'))
})
