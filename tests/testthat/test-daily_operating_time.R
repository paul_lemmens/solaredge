test_that("Production start/end are retrieved correctly", {

  fn <- 'test_data.csv'
  d <- solaredge_process_panel_data(fn, tz = 'UTC')

  expect <- tibble::tribble(
    ~dtstamp, ~dstamp, ~panel_id, ~power, ~timepoint,
    "2023-05-14 06:30:00+00:00", '2023-05-14', '233376873', 3.58, 'production_start',
    "2023-05-14 21:00:00+00:00", '2023-05-14', '233376873', 3.38, 'production_end',
    "2023-05-15 06:30:00+00:00", '2023-05-15', '233376873', 3.77, 'production_start',
    "2023-05-15 21:30:00+00:00", '2023-05-15', '233376873', 2.75, 'production_end',
    "2023-05-16 06:30:00+00:00", '2023-05-16', '233376873', 3.34, 'production_start',
    "2023-05-16 21:15:00+00:00", '2023-05-16', '233376873', 3.72, 'production_end',
    "2023-05-17 06:15:00+00:00", '2023-05-17', '233376873', 1.47, 'production_start',
    "2023-05-17 21:30:00+00:00", '2023-05-17', '233376873', 3.39, 'production_end',
    "2023-05-18 06:15:00+00:00", '2023-05-18', '233376873', 1.79, 'production_start',
    "2023-05-18 21:15:00+00:00", '2023-05-18', '233376873', 3.03, 'production_end',
    "2023-05-19 06:30:00+00:00", '2023-05-19', '233376873', 3.96, 'production_start',
    "2023-05-19 21:30:00+00:00", '2023-05-19', '233376873', 2.25, 'production_end',
    "2023-05-20 06:30:00+00:00", '2023-05-20', '233376873', 3.06, 'production_start',
    "2023-05-20 14:45:00+00:00", '2023-05-20', '233376873', 4.69, 'production_end',
  ) %>%
    dplyr::mutate(dtstamp = lubridate::ymd_hms(dtstamp),
                  dstamp  = lubridate::ymd(dstamp))

  res <- solaredge_daily_production_period(d, identifier = panel_id) %>%
    dplyr::filter(panel_id == '233376873')

  expect_equal(res, expect)
})
