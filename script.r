library(httr2)
library(jsonlite)
library(tidyverse)
library(lemmens)

## options(pillar.subtle = FALSE)
options(rlang_backtrace_on_error = "none")

base_url <- 'https://monitoringapi.solaredge.com/'

req <- 'sites/list'
res <- GET(paste0(base_url, req), query = list(api_key = api_key))
site <- fromJSON(content(res, as = 'text'), flatten = TRUE)
site <- site$sites$site

req <- paste0('site/', site$id, '/energy')
res <- GET(paste0(base_url, req), query = list(api_key = api_key, timeUnit = 'QUARTER_OF_AN_HOUR',
                                               startDate = '2023-04-13', endDate = '2023-04-30'))
energy <- fromJSON(content(res, as = 'text'), flatten = TRUE)
energy <- energy$energy$values %>%
  mutate(date = lubridate::as_datetime(date),
         unit = energy$unit,
         time_unit = energy$timeUnit,
         measured_by = energy$measuredBy)
p1 <- energy %>%
  ggplot(mapping = aes(x = date, y = value)) +
    geom_line(mapping = aes(group = 1)) +
    scale_x_datetime(date_breaks = 'day', date_minor_breaks = '4 hours') +
    theme_lemmens + theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5))
plot(p1)

req <- paste0('site/', site$id, '/power')
res <- GET(paste0(base_url, req), query = list(api_key = api_key, startTime = '2023-04-13 0:00:00',
                                               endTime = '2023-04-30 23:59:59'))
power <- fromJSON(content(res, as = 'text'), flatten = TRUE)
power <- power$power$values %>%
  mutate(date = lubridate::as_datetime(date),
         unit = power$unit,
         time_unit = power$timeUnit,
         measured_by = power$measuredBy)
p2 <- p1 %+% power
print(p2)

library(patchwork)
p1 / p2

## Maar nu rare lijn op nul. Uitzoeken dat dit uit nieuwe data komt en oplossing zoeken.
## durf ik alle nullen weg te halen?
##
## 4, 6, 11, 13 aug. clipten? Data in de app zijn redelijk ok, dus wellicht dat 't een
## verbindingsprobleem is geweest.

library(tidyverse)
library(lemmens)
devtools::load_all()

panel_dat <- readRDS(file = '~/Nextcloud/solaredge/panel_data_2023-11-10T21-37-22.rds')

panel_dat <- process_new_panel_data(old_data_file = '~/Nextcloud/solaredge/panel_data_2023-11-10T21-37-22.rds',
                                    new_data_dir = '~/Nextcloud/solaredge/')

panel_dat %>%
  mutate(dstamp = lubridate::as_date(dtstamp)) %>%
  filter(type == 'panel') %>%
  solaredge_plot_panel_production(identifier = panel_name)

## Calculate start moment per panel per day
d <- panel_dat %>%
  solaredge_daily_production_period()
solaredge_plot_production_period(d)
solaredge_plot_production_period(d, variant = 'panel')

## cumulative
solaredge_plot_cumulative_energy()
solaredge_plot_panel_cumulative_energy(panel_dat, color = 'lightblue')

panel_names <- c('inverter', 'string', 'kapel z', 'dak z lb', 'dak z r', 'garage o', 'dak o lo', 'dak z ro',
                 'garage achter', 'dak o ro', 'garage voor', 'garage w', 'dak o rb', 'garage mid', 'dak z rb',
                 'dak z lo', 'kapel n'))
