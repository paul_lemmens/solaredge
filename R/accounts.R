#' @title Return Account and Sub-accounts Connected to API token
#'
#' @description
#' Provides an overview of the accounts connect to an API
#' token, possibly using URL parameters for filtering, sorting, and
#' pagination. Note that this endpoint provides information as the
#' [`solaredge_site_list()`] and [`solaredge_site_details()`]
#' endpoints with the exception that the latter to have the PTO date
#' which the present does not have (permission to operate).
#'
#' @inheritParams solaredge_site_list
#' @param size The maximum number of accounts to be returned. Defaults to
#'    100 (integer value).
#' @param start_index First account to be returned in the results
#'    (integer).
#' @param search_text Search string to filter accounts on. Searches in the
#'    account name, notes, email, country and state, city, zip, and full
#'    address (string).
#' @param sort_property One of name, country, city, address, zip, fax,
#'    phone, or notes to sort the results by (string).
#' @param sort_order Indicator for sorting ascending (ASC) or descending
#'    (DESC). A string with ASC as default.
#'
#' @return A [tibble][tibble::tibble-package] with the account ID, name, location,
#'    company website, contact person, account details, and parent
#'    account.
#'
#' @export
solaredge_account_list <- function(collection = 'accounts',
                                   size = NULL, start_index = NULL,
                                   search_text = NULL,
                                   sort_property = c('name', 'country', 'city',
                                                     'address', 'zip', 'fax',
                                                     'phone', 'notes'),
                                   sort_order = c('ASC', 'DESC')) {

  sort_property <- match.arg(sort_property)
  sort_order <- match.arg(sort_order)

  res <- solaredge_api(collection = collection, resource = 'list',
                       size = size, startIndex = start_index,
                       searchText = search_text, sortProperty = sort_property,
                       sortOrder = sort_order)
  ## TODO: another area where a mutiple of results has not been tested for.
  res <- res$sites$site
  res <- res %>% tidyr::unnest(cols = c(location, primaryModule, uris, publicSettings)) %>%
    dplyr::rename_with(.fn = snakecase::to_snake_case)

  return(res)
}
