#' @title Plot Panels' Cumulative Energy Production
#'
#' @description
#' Calculates, per panel, the cumulative energy producted to date and
#' then plots this, possibly overlaid with a fitted model to estimate
#' total yearly production.
#'
#' @param dfr A data frame / tibble produced by [`solaredge_process_panel_data()`].
#' @inheritParams solaredge_plot_production_period
#' @param smooth Boolean to indicate whether a partially transparent general
#'    logistic smoother should be drawn (alpha == 0.3). Defaults to TRUE.
#' @param smoother_fun A function that returns a suitably tailored
#'    `geom_smooth` function to overlay on top of the cumulative data.
#'    Defaults to the package internal [`cumulative_smoother()`].
#' @param ... Additional parameters that are passed onto other functions.
#'
#' @return a [ggplot2][ggplot2::ggplot2-package] object that can be
#'    further tuned.
#'
#' @export
# nocov start
solaredge_plot_panel_cumulative_energy <- function(dfr, identifier = panel_name, smooth = TRUE,
                                                   smoother_fn = cumulative_smoother,
                                                   ...) {

  dat <- dfr %>%
    dplyr::mutate(year = lubridate::year(dtstamp),
                  week = lubridate::week(dtstamp)) %>%
    dplyr::arrange({{ identifier }}, year, week)

  ## We know that dfr is expressed in 15 min. intervals, so compute energy from
  ## power by dividing by 4.
  dat <- dat %>%
    dplyr::mutate(power = power / 4)

  dat <- dat %>%
    dplyr::group_by({{ identifier }}, year, week) %>%
      dplyr::summarize(power = sum(power, na.rm = TRUE)) %>%
    dplyr::ungroup() %>%
    dplyr::mutate(power_ctd = cumsum(tidyr::replace_na(power, replace = 0)),
                  .by = c({{ identifier }}, year)) %>%
    dplyr::mutate(power_ctd = power_ctd / 1000)

  p <- dat %>%
    dplyr::filter(!{{ identifier }} %in% c('string', 'inverter')) %>%
    dplyr::mutate(year = factor(year)) %>%
    ggplot2::ggplot(mapping = ggplot2::aes(x = week, y = power_ctd, colour = year)) +
      ggplot2::geom_line(ggplot2::aes(group = 1)) +
      ggplot2::facet_wrap(dplyr::vars( {{ identifier }} )) +
      ggplot2::scale_x_continuous(limits = c(1, 53)) +
      ggplot2::labs(x = '\nWeek in year', y = 'Energy production (kWh)\n',
                    title = 'Cumulative energy over the year\n',
                    colour = 'Year')

  if (smooth) {
    p <- p +
      ## Work from this example with package drc (dose-response curves). Use
      ## a 3 parameter function to ensure lower asymptote remains at zero.
      ## https://stackoverflow.com/a/43564372/503175
      smoother_fn(...)
  }

  return(p)

}
# nocov end


#' @title Plot Cumulative Energy
#'
#' @description
#' Calculate cumulative energy over time (in periods of weeks) and plot
#' this per available year with the aim to spot differences between
#' years that might reflect panel deterioration.
#'
#' @param time_unit Time unit in which to express the energy measurements.
#'    By default set to day so that the full year can be downloaded in
#'    one go.
#' @param smooth Boolean to indicate whether a partially transparent linear
#'    smoother should be drawn (alpha == 0.3). Defaults to TRUE.
#' @inheritParams solaredge_plot_panel_cumulative_energy
#'
#' @return A `ggplot2` object that can be further tweaked for personal
#'    visual theming.
#'
#' @export
# nocov start
solaredge_plot_cumulative_energy <- function(time_unit = '1d', smooth = TRUE,
                                             smoother_fn = cumulative_smoother,
                                             ...) {

  production_dates <- solaredge_production_dates()
  dat <- solaredge_site_energy(start_date = production_dates$start_date,
                               end_date   = production_dates$end_date,
                               time_unit  = time_unit) %>%
    dplyr::mutate(year = lubridate::year(date),
                  week = lubridate::week(date)) %>%
    dplyr::arrange(year, week)

  dat <- dat %>%
    dplyr::group_by(year, week) %>%
      dplyr::summarize(energy = sum(energy, na.rm = TRUE)) %>%
    dplyr::ungroup() %>%
    dplyr::mutate(energy_ctd = cumsum(tidyr::replace_na(energy, replace = 0)),
                  .by = year) %>%
    dplyr::mutate(energy_ctd = energy_ctd / 1000)

  p <- dat %>%
    dplyr::mutate(year = factor(year)) %>%
    ggplot2::ggplot(mapping = ggplot2::aes(x = week, y = energy_ctd, colour = year)) +
      ggplot2::geom_line(ggplot2::aes(group = 1)) +
      ggplot2::scale_x_continuous(limits = c(1, 53)) +
      ggplot2::labs(x = '\nWeek in year', y = 'Energy production (kWh)\n',
                    title = 'Cumulative energy over the year\n',
                    colour = 'Year')

  if (smooth) {
    p <- p +
      smoother_fn(...)
  }

  return(p)
}
# nocov end
