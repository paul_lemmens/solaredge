#' @title Download Individual Panels' Data
#'
#' @description
#' The SolarEdge owner's web interface (can) show
#' the production of individual panels that belong to a site.
#' These data are, unfortunately, not exposed via an official
#' API endpoint. Additionally, /these data cannot be downloaded,
#' retrospectively, beyond the preceding week/. Therefore, it is important
#' that you create a solution yourself to repeatedly download the data
#' from the last week and store those data locally.
#'
#' Various solutions can be found on the web (1-3) of which
#' (3) proved to be easiest to work with by the author of the
#' package. This function copies the Python script to a user specified
#' location for local adaptation.
#'
#' The URL that is called can be configured to download data on a daily or
#' weekly basis. Compared to the open/documented API endpoints there is no way
#' to retrieve historic data. Therefore the users must regularly download the
#' data. The author suggests to setup a cron job using this script to regularly
#' download the data. The suggested frequency is to run the job every 5 or at
#' most every 6 days. This results in a small amount of duplicate data that
#' [`solaredge_process_panel_data()`] takes care of. For this reason, by
#' default, the script is set to download the last week's worth of data.
#'
#' The python script needs three API related parameters: the user id,
#' password, and the API key. It expects that these are provided through
#' environment variables wherein it is the user's responsibility that this
#' done in a safe way. /Additionally, you may need to log into the
#' SolarEdge monitoring portal to make this function work./
#'
#' It creates a data frame / table with rows for each of the 15 min.
#' time periods and columns for the individual panels as well as the
#' inverter. [`solaredge_process_panel_data()`] can be used to process
#' this data into a format that is uniform with the other data from
#' this package.
#'
#' @param destination A string with a file system path to copy the script to.
#'
#' @return Invisible.
#'
#' @references
#'
#' 1. https://github.com/dragoshenron/solaredge-webscrape/blob/main/solaredge_webscrape_influxdb.sh
#'
#' 2. https://twitter.com/AyrtonBourn/status/1405071870026731523
#'
#' 3. https://gist.github.com/cooldil/0b2c5ee22befbbfcdefd06c9cf2b7a98
#'
#' @export
# nocov start
copy_panel_module <- function(destination) {

  stopifnot(file.exists(destination))

  file.copy(from = system.file('python', 'solaredge_panel_data.py',
                               package = 'solaredge'),
            to = destination)

  return(invisible())
}
# nocov end

#' @title Process Individual Panels' Data
#'
#' @description
#' The script detailed in [`copy_panel_module()`] writes files
#' with data that need further processing to make its format compliant
#' to the other functions in the package as well as to clean (in
#' particular) duplicates.
#'
#' @param fn Path and filename of the file with output data from the
#'    Python script.
#' @param tz Time zone in which the data was downloaded. A string that
#'    defaults to 'Europe/Amsterdam'. Note that the Python script also
#'    explicitly sets Europe/Amsterdam as tz for the data and that these
#'    tz need to align.
#'
#' @return A long [tibble][tibble::tibble-package] with colums for the date/time stamp, the
#'    panel id number, and the produced power (in W).
#'
#'
#' @export
solaredge_process_panel_data <- function(fn, tz = 'Europe/Amsterdam') {

  dat <- readr::read_csv(file = fn, locale = readr::locale(tz = tz), show_col_types = FALSE) %>%
    dplyr::rename(dtstamp = `...1`) %>%
    tidyr::pivot_longer(cols = -dtstamp) %>%
    dplyr::rename(panel_id = name, power = value) %>%
    dplyr::mutate(unit = 'W')

  ## Because calling the python script repeatedly can cause data duplication,
  ## work on distinct data only.
  ## FIXME: this isn't correct: a single file can not contain duplicates!
  dat <- dat %>%
    dplyr::distinct(dtstamp, panel_id, .keep_all = TRUE)

  return(dat)
}


#' @title Panel Production Over The Day
#'
#' @description
#' Visualizes the production of individual panels over the the day.
#'
#' @param dfr Data frame / tibble with the production data per panel
#'    organized as having a `dtstamp`, `panel_id` (see below), and `power`.
#' @param identifier Unquoted (NSE) variable name by which to organize the
#'    data (in facets per PV panel). Defaults to `panel_id` which is the
#'    panels' id from the API.
#' @param smooth Boolean to indicate whether a [`ggplot2::geom_smooth()`]
#'    layer should be added to the plot; Defaults to `TRUE`.
#'
#' @return A ggplot2 object that can be further tweaked.
#'
#'
#' @export
# nocov start
solaredge_plot_panel_production <- function(dfr, identifier = panel_id, smooth = TRUE) {

  ## stopifnot(names(dfr) %in% c('dtstamp', 'power', rlang::as_string({{ identifier }} )))

  p <- dfr %>%
    dplyr::mutate(tstamp = hms::as_hms(dtstamp),
                  dstamp = lubridate::as_date(dtstamp),
                  month  = factor(lubridate::month(dtstamp), levels = 1:12,
                                  labels = month.abb)) %>%
    ggplot2::ggplot(mapping = ggplot2::aes(x = tstamp, y = power, color = month)) +
      ggplot2::geom_line(mapping = ggplot2::aes(group = dstamp), alpha = 0.1) +
      ggplot2::facet_wrap(dplyr::vars({{ identifier }}), scales = 'free_y') +
      ggplot2::scale_x_time(labels = scales::label_time(format = '%H:%M')) +
      ggplot2::theme(axis.text.x = ggplot2::element_text(size = ggplot2::rel(0.6)))

  if (smooth) {
    p <- p + ggplot2::geom_smooth(se = FALSE)
  }

  return(p)
}
# nocov end

#' @title Get Panel Information
#'
#' @description
#' Another non-exposed API endpoint can be used to extract
#' information about the individual panels that enables, for instance,
#' to set more meaningful names for the panels than just the
#' `panel_id`'s extracted by the Python script ([`copy_panel_module()`]).
#'
#' This function also builds on Python but uses a simple username and
#' password authentication.
#'
#' Note that this function is not tested nor designed for setups that have
#' multiple strings. If you have this setup, use this function's code as
#' inspiration to write code dedicated to your setup.
#'
#' @section Way of working:
#' The proposed way of working around setting custom panel names, is to
#' first download the panel information. By listing the `panel_id`s, you
#' can then review the location of individual panels in the layout section
#' of the app or the web dashboard.
#'
#' Using, for instance, this location info, you can then create a vector
#' of panel names in the order of the `panel_id`s that you can provide as
#' parameter to a second run of this function.
#'
#' @param panel_names A vector of strings to identify panels in a human-
#'    readable way. Should have the same length as there are PV panels,
#'    inverter(s), and strings in your setup, *and you are responsible for
#'    ensuring the correct order of the names*. Defaults to `NULL` to ensure
#'    that panel names are optional.
#' @param path A directory name (can be relative or absolute path) where
#'    to store the downloaded data. Defaults to current working directory.
#'
#' @return A [tibble][tibble::tibble-package] with serial number, name,
#'    display name, order, type (inverter, string, or panel/powerbox),
#'    and operations key.
#'
#'    When `panel_names` is specified,
#'    an additional variable `panel_name` is added to the tibble.
#'    Note that in this case, this tibble can/should be merged
#'    (on `panel_id`) with the data generated by the Python script
#'    "exposed" by [`copy_panel_module()`].
#'
#' @export
solaredge_panel_info <- function(panel_names = NULL, path = '.') {

  if (!dir.exists(path)) {
    stop('Destination path in solaredge_panel_info() does not exist.')
  }

  panel_info <- panel_module$download_panel_layout
  d <- jsonlite::fromJSON(panel_info(dest_path = path))

  inverter_info <- purrr::chuck(d$logicalTree, 'children', 'data')
  string_info <- purrr::chuck(d$logicalTree, 'children', 'children', 1, 'data')
  panel_info <- purrr::chuck(d$logicalTree, 'children', 'children', 1, 'children', 1, 'data')

  pv_info <- dplyr::bind_rows(inverter_info, string_info, panel_info) %>%
    dplyr::rename_with(.fn = snakecase::to_snake_case) %>%
    dplyr::mutate(type = tolower(type)) %>%
    dplyr::mutate(type = dplyr::if_else(type == 'power_box', 'panel', type)) %>%
    dplyr::rename(panel_id = id)

  if (!is.null(panel_names)) {
    if (length(panel_names) != nrow(pv_info)) {
      warning(paste0('Number of panel names (', length(panel_names), ') is different than number of panels (', nrow(pv_info), ').\n')) #nolint
    }
    pv_info <- pv_info %>%
      dplyr::mutate(panel_name = panel_names)
  }

  return(pv_info)
}


#' @title Calculate Daily Production Time
#'
#' @description
#' With the per panel data it is possible to "estimate", for each day of
#' data, when a panel starts and stops producing electricity. This data
#' can provide insight into whether particular panels are underperforming
#' due to their placement. Additionally, it provides insight how the
#' production period changes over the year given the sun's rise time and
#' position in the sky.
#'
#' This function presumes that a `dtstamp` variable exists which is a
#' date and time stamp for each measured power production.
#'
#' Also note that the function, for simple reasons of easy code, presumes
#' that the end of the production is the latest time stamp (on a given day)
#' during which the production still is higher than the criterion.
#'
#' The function also makes the presumption that solar panel production occurs
#' during the day (say from 8am to 4pm) so that a simple extraction of the
#' date from the date stamp is sufficient to loop over days.
#'
#' @param dfr Data frame / tibble that is, typically, generated by a call
#'    to [`copy_panel_data`].
#' @param identifier A (NSE) variable that identifies individual panels
#'    in `dfr`.
#' @param criterion A value that defines the delta/change between a panel
#'    that is not production versus when it starts producing power. Defaults
#'    to 1W.
#'
#' @return A tibble with the start and end time (time stamp) production
#'    per panel identified by `identifier`.
#'
#' @export
solaredge_daily_production_period <- function(dfr, identifier = panel_name,
                                           criterion = 1) {

  ## Extract simple date from date/time stamp and remove all production values
  ## smaller than the criterion.
  res <- dfr %>%
    dplyr::mutate(dstamp = lubridate::as_date(dtstamp)) %>%
    dplyr::filter(power >= criterion) %>%
    dplyr::arrange({{ identifier }}, dtstamp)

  ## Grouping by panel and date, select first and last value of the grouped
  ## tibble to get to start and end of production. Because the slice()s
  ## return full data frame, do this in two steps.
  production_start <- res %>%
    dplyr::group_by({{ identifier }}, dstamp) %>%
      dplyr::slice_head(n = 1)

  production_end <- res %>%
    dplyr::group_by({{ identifier }}, dstamp) %>%
      dplyr::slice_tail(n = 1)

  res <- dplyr::bind_rows(list(production_start = production_start,
                               production_end = production_end),
                          .id = 'timepoint') %>%
    dplyr::select(dtstamp, dstamp, {{ identifier }}, power, timepoint) %>%
    dplyr::ungroup() %>%
    dplyr::arrange({{ identifier }}, dtstamp)

  return(res)
}


#' @title Plot Production Times
#'
#' @description
#' Visualizes the data from [`solaredge_daily_production_period()`] in a
#' version with weeks on the *x*-axis or one with separate panels per
#' week to illustrate possible differences in starting and stopping
#' between panels. The function calculates average start and end
#' times over the days of the week.
#'
#' @param dfr Data frame / tibble resulting from [`solaredge_daily_production_period()`].
#' @param variant One of 'week' or 'panel' to indicate method of facetting.
#' @param identifier Variable in `dfr` that identifies individual patterns.
#'
#' @return A ggplot2 object that can be further tuned.
#'
#' @export
# nocov start
solaredge_plot_production_period <- function(dfr, variant = c('week', 'panel'),
                                             identifier = panel_name) {

  variant <- match.arg(variant)

  ## Calculate the averages and standard deviation.
  dat <- dfr %>%
    dplyr::mutate(tstamp = hms::as_hms(dtstamp),
                  wknr   = lubridate::isoweek(dtstamp),
                  year   = lubridate::year(dtstamp),
                  tmp    = as.numeric(tstamp)) %>%
    dplyr::group_by(panel_name, year, wknr, timepoint) %>%
      dplyr::summarize(avg       = lubridate::seconds_to_period(mean(as.numeric(tstamp))),
                       start_sd  = lubridate::seconds_to_period(sd(as.numeric(tstamp))),
                       ymin      = seconds_to_period(as.duration(avg) - dminutes(start_sd)),
                       ymax      = seconds_to_period(as.period(as.duration(avg) + dminutes(start_sd))),
                       start_avg = ymd_hms(paste("1970-1-1", avg)),
                       lower     = ymd_hms(paste("1970-1-1", ymin)),
                       upper     = ymd_hms(paste("1970-1-1", ymax))) %>%
    dplyr::ungroup()

  if (variant == 'week') {
    p <- dat %>%
      ggplot2::ggplot(mapping = aes(x = {{ identifier }}, y = start_avg, color = timepoint)) +
      ggplot2::geom_pointrange(mapping = ggplot2::aes(ymax = upper, ymin = lower), size = 0.1) +
      ggplot2::facet_wrap(~ wknr, scales = 'free_y') +
      ggplot2::theme(axis.text.x = ggplot2::element_text(angle = 90, hjust = 1, vjust = 0.5))

  } else if (variant == 'panel') {
    p <- dat %>%
      ggplot2::ggplot(mapping = aes(x = wknr, y = start_avg, color = timepoint)) +
      ggplot2::geom_line(mapping = ggplot2::aes(group = wknr), color = 'black', alpha = 0.2) +
      ggplot2::geom_pointrange(mapping = ggplot2::aes(ymax = upper, ymin = lower), size = 0.1) +
      ggplot2::facet_wrap(dplyr::vars({{ identifier }}), scales = 'free_y')

  } else {
    stop(paste('Do not know how to draw', variant, 'variant'))
  }

  lims <- lubridate::ymd_hms(c('1970-1-1 0:0:0', '1970-1-1 23:59:59'))
  p <- p + scale_y_datetime(date_labels = "%H:%M", date_minor_breaks = '2 hours',
                            limits = lims)

  return(p)
}
# nocov end
